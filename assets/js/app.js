// Eliminar de Local Storage
localStorage.clear();
//creamos la base de datos de comentarios

var twits = localStorage.getItem("comentarios"); //Obtener datos de localStorage
arreglo = JSON.parse(twits); // Covertir a objeto
if (arreglo === null)
  // Si no existe, creamos un array vacio.
  arreglo = [];
function Mensaje(t) {
  switch (t) {
    case 1: //
      $(".mensaje-alerta").append("Se agrego el comentario con exito");
      break;
    case 2: //
      $(".mensaje-alerta").append("No se agrego el comentario con éxito");
      break;
    default:
  }
}

function Agregar() {
  // Seleccionamos los datos de los inputs de formulario
  var cliente = JSON.stringify({
    twits: $("#tweet").val()    
  });



  arreglo.push(cliente); // Guardar datos en el array definido globalmente
  localStorage.setItem("comentarios", JSON.stringify(arreglo));
  Listar();
  return Mensaje(1);
}
function Listar() {
  $("#lista-tweets").html(
    "<thead>" +
      "<tr>" +
      "<th> No </th>" +
      "<th> Tweet </th>" +
      "<th>Accion</th>" +    
      "</tr>" +
      "</thead>" +
      "<tbody>" +
      "</tbody>"
  );

  var indice = 0;
  for (var i in arreglo) {
    var d = JSON.parse(arreglo[i]);

    $("#lista-tweets").append(
      "<tr>" +
        "<td>" +
        (indice + 1) +
        "</td>" +
        "<td>"+ d.twits+      
        "</td>" +    
        "<td>" +
        "<input type=button Onclick=Eliminar('"+indice+"') value=Eliminar class='mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect'/> "+
        "</td>" + 

        "</tr>"
    );
    indice++;
  }
  if(indice==0)
  {
    $("#lista-tweets").empty();
    $("#lista-tweets").append('<h4>No hay Tweets para mostrar </h4>')
  }
};

function Eliminar(id)
{  
    arreglo.splice(id, 1);
	localStorage.setItem("comentarios", JSON.stringify(twits));
	alert("Comentario eliminado.");
 
    Listar();

};
